//
//  InitInteractor.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

class InitInteractor: InitInteractorProtocol {
    weak var presenter: InitPresenterProtocol?
    
    func getToken(index: Int) {
        guard let apiURL = URL(string: "https://api.themoviedb.org/3/authentication/token/new?api_key=641f683f7f3e37d02c0392e05520931d") else { return }
        var request = URLRequest(url: apiURL)
        
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                if data != nil {
                    let contetResponse = try JSONDecoder().decode(AuthToken.self, from: data!)
                    self.presenter?.onSuccessGetToken(data: contetResponse, response: (response as! HTTPURLResponse), index: index)
                    print(contetResponse)
                }
                
            }catch{
                print("Error get token", error, error.localizedDescription)
                self.presenter?.onFaillGetToken(error: error)
            }
        }.resume()
        
    }
    
    func getTokenLogin(index: Int, token: String) {
        guard let apiURL = URL(string: "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=641f683f7f3e37d02c0392e05520931d") else { return }
        var request = URLRequest(url: apiURL)
        let parametros : [String : String] = [
            "username" : "PZamudio",
            "password" : "SonsOfAnarchy22",
            "request_token" : token
        ]
        let body = try! JSONSerialization.data(withJSONObject: parametros)
        
        request.httpMethod = "POST"
        request.httpBody = body
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                if data != nil {
                    let contetResponse = try JSONDecoder().decode(AuthToken.self, from: data!)
                    self.presenter?.onSuccessGetToken(data: contetResponse, response: (response as! HTTPURLResponse), index: index)
                    print(contetResponse)
                }
                
            }catch{
                print("Error get token", error, error.localizedDescription)
                self.presenter?.onFaillGetToken(error: error)
            }
        }.resume()
        
    }
    
    func getSessionID(requestToken: String) {
        guard let apiURL = URL(string: "https://api.themoviedb.org/3/authentication/session/new?api_key=641f683f7f3e37d02c0392e05520931d") else { return }
        var request = URLRequest(url: apiURL)
        let parametros : [String : String] = [
            "request_token" : requestToken
        ]
        let body = try! JSONSerialization.data(withJSONObject: parametros)
        
        request.httpMethod = "POST"
        request.httpBody = body
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                if data != nil {
                    let contentReposne = try JSONDecoder().decode(SessionIDResponse.self, from: data!)
                    self.presenter?.onSuccessSessionID(data: contentReposne, response: (response as! HTTPURLResponse))
                }
                
            }catch{
                print("Error get session ID", error, error.localizedDescription)
                self.presenter?.onFailSuccessID(error: error)
                
            }
        }.resume()
        
    }
    
}
