//
//  InitViewController.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import UIKit
import SystemConfiguration
import CoreData

class InitViewController: UIViewController, InitViewProtocol {
    
// MARK: PROTOCOL VAR -
    var presenter: InitPresenterProtocol?
    
// MARK: - FUNCION CORE DATA
    func conexion()-> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }

// MARK: LIFE CYCLE VIEW FUNCTIONS -
    override func viewDidLoad() {
        super.viewDidLoad()
        validateConnection()
        
    }
    
// MARK: VALIDATE FUNCTIONS -
    private func validateConnection() {
        if CheckInternet.Connection() {
            presenter?.requestAuthToken(index: 0)
    
        }else{
            validateDataSave()
        }
    }
    
// MARK: COREDATA FUNCTIONS -
    func deleteDB() {
        let contexto = conexion()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MoviesDB")
        let borrar = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try contexto.execute(borrar)
            print("Exito al borrar DB")
        } catch let error as NSError{
            print("Error al eliminar DB", error.localizedDescription)
        }
    }
    
    
    
    func validateDataSave() {
        let contexto = self.conexion()
        let fetchRequest : NSFetchRequest<MoviesDB> = MoviesDB.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            
            print("number of movies: \(resultados.count)")
            if resultados.count == 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.showAlert(title: "Aviso", text: "No cuentas con conexión a internet", btnTitle: "Entendido")
                }
            }else{
                let module = ListRouter.getController(sessionID: "MovieDB")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.navigationController?.pushViewController(module, animated: true)
                }

            }
           
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
        }
    }
    
    
// MARK: SETUP FUNCTIONS -
    func showAlert(title: String, text: String, btnTitle: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let btn = UIAlertAction(title: btnTitle, style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(btn)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: AUTH SERVICES RESPONSE -
extension InitViewController {
    func succesGetAuthToken(data: AuthToken, index: Int) {
        switch index {
        case 0:
            print("index 0")
            presenter?.requestAuthLogin(index: 1, token: data.request_token ?? "")
        case 1:
            print("index 1")
            presenter?.requestSessionID(requestToken: data.request_token ?? "")
        default:
            print("default")
        }
    }
    
    func failGetAuthToken(message: String) {
        print(message)
    }
    
    func succesGetSessionID(data: SessionIDResponse) {
        print(data)
        let module = ListRouter.getController(sessionID: data.session_id ?? "")
        self.navigationController?.pushViewController(module, animated: true)
    }
    
    func failGetSessionID(message: String) {
        print(message)
    }
    
}
