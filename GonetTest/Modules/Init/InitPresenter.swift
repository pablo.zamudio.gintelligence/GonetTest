//
//  InitPresenter.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

class InitPresenter: InitPresenterProtocol {
    
    weak private var view: InitViewProtocol?
    var interactor: InitInteractorProtocol?
    private let router: InitRouterProtocol?
    
    init(interface: InitViewProtocol, interactor: InitInteractorProtocol?, router: InitRouterProtocol ) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
// MARK: AUTH TOKEN -
    func requestAuthToken(index: Int) {
        interactor?.getToken(index: index)
    }
    
    func onSuccessGetToken(data: AuthToken, response: HTTPURLResponse, index: Int) {
        DispatchQueue.main.async {
            if response.statusCode == 200 {
                self.view?.succesGetAuthToken(data: data, index: index)
            }else{
                self.view?.failGetAuthToken(message: "Error unknown")
            }
        }
    }
    
    func onFaillGetToken(error: Error) {
        DispatchQueue.main.async {
            self.view?.failGetAuthToken(message: error.localizedDescription)
        }
    }
    
// MARK: AUTH WITH LOGIN TOKEN -
    func requestAuthLogin(index: Int, token: String) {
        interactor?.getTokenLogin(index: index, token: token)
    }
    
// MARK: SESSION ID -
    func requestSessionID(requestToken: String) {
        interactor?.getSessionID(requestToken: requestToken)
    }
    
    func onSuccessSessionID(data: SessionIDResponse, response: HTTPURLResponse) {
        DispatchQueue.main.async {
            if response.statusCode == 200 {
                self.view?.succesGetSessionID(data: data)
            }else{
                self.view?.failGetSessionID(message: "Error unknown")
            }
        }
    }
    
    func onFailSuccessID(error: Error) {
        DispatchQueue.main.async {
            self.view?.failGetAuthToken(message: error.localizedDescription)
        }
    }
    
}
