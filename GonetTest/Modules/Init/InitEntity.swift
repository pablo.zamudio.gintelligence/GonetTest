//
//  InitEntity.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

struct AuthToken: Codable {
    var success: Bool?
    var expires_at: String?
    var request_token: String?
}

struct SessionIDResponse: Codable {
    var success: Bool?
    var session_id: String?
}
