//
//  InitProtocols.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

// MARK: Wireframe -
protocol InitRouterProtocol: class {
    
}

// MARK: Presenter -
protocol InitPresenterProtocol: class {
    func requestAuthToken(index: Int)
    func onSuccessGetToken(data: AuthToken, response: HTTPURLResponse, index: Int)
    func onFaillGetToken(error: Error)
    
    func requestAuthLogin(index: Int, token: String)
    
    func requestSessionID(requestToken: String)
    func onSuccessSessionID(data: SessionIDResponse, response: HTTPURLResponse)
    func onFailSuccessID(error: Error)
}

// MARK: Interactor -
protocol InitInteractorProtocol: class {
    var presenter: InitPresenterProtocol? { get set }
    
    func getToken(index: Int)
    func getTokenLogin(index: Int, token: String)
    func getSessionID(requestToken: String)
}

// MARK:  VIEW -
protocol InitViewProtocol: class {
    var presenter: InitPresenterProtocol? { get set }
    
    func succesGetAuthToken(data: AuthToken, index: Int)
    func failGetAuthToken(message: String)
    
    func succesGetSessionID(data: SessionIDResponse)
    func failGetSessionID(message: String)
    
}
