//
//  DetailInteractor.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import Foundation

class DetailInteractor: DetailInteractorProtocol {
    weak var presenter: DetailPresenterProtocol?
    
    func getDetail(movieID: Int, type: String) {
        guard let apiURL = URL(string: "https://api.themoviedb.org/3/\(type)/\(movieID)?api_key=641f683f7f3e37d02c0392e05520931d&language=en-US") else { return }
        var request = URLRequest(url: apiURL)
        
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            print(response)
            do {
                if data != nil {
                    let contetResponse = try JSONDecoder().decode(DetailResponse.self, from: data!)
                    self.presenter?.onSuccessGetDetail(data: contetResponse, response: (response as! HTTPURLResponse), type: type)
                }
                
            }catch{
                print("Error get token", error, error.localizedDescription)
                self.presenter?.onFailGetDetail(error: error)
            }
        }.resume()
    }
}
