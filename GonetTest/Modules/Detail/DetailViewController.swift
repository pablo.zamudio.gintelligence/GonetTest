//
//  DetailViewController.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import UIKit

class DetailViewController: UIViewController, DetailViewProtocol {
    
// MARK: PROTOCOL VAR -
    var presenter: DetailPresenterProtocol?

// MARK: @IBOUTLTES -
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    
// MARK: LOCAL VAR -
    var movieID = 0
    var typeObject = "Unspecified"
    
// MARK: LIFE CYCLE VIEW FUNCTION -
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.requestDetail(moviesID: movieID, type: typeObject)
    }

}

// MARK: DETAIL SERVICE -
extension DetailViewController {
    func successGetDetail(data: DetailResponse, type: String) {
        print(data)
        mainImg.DownloadStaticImage("https://image.tmdb.org/t/p/w500\(data.poster_path ?? "")")
        
        switch type {
        case "movie":
            lblMovieName.text = data.title
            let newDate = data.release_date?.getDate(originalFormat: "yyyy/MM/dd")
            let strDate = newDate?.getDateStr(newFormatt: "MMM d, yyyy")
            lblDate.text = "(\(strDate?.uppercased() ?? ""))"
            
        case "tv":
            lblMovieName.text = data.name
            let newDate = data.first_air_date?.getDate(originalFormat: "yyyy/MM/dd")
            let strDate = newDate?.getDateStr(newFormatt: "MMM d, yyyy")
            lblDate.text = "(\(strDate?.uppercased() ?? ""))"
            
        default:
            break
        }       
        lblOverview.text = data.overview
        var stringArray = [String]()
        data.genres?.forEach({ (gen) in
            stringArray.append(gen.name ?? "")
        })
        let string = stringArray.joined(separator: ", ")
        lblGenre.text = string
    }
    
    func failGetDetail(message: String) {
        print(message)
    }
}
