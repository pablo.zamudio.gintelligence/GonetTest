//
//  DetailRouter.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import Foundation
import UIKit

open class DetailRouter: DetailRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static public func getController(movieID: Int, type: String) -> UIViewController {
        
       // Generating module components
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: InitViewController.self))
        let view: DetailViewController = storyboard.instantiateViewController(identifier: "DetailView") as! DetailViewController
        let interactor = DetailInteractor()
        let router = DetailRouter()
        let presenter = DetailPresenter(interface: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        view.movieID = movieID
        view.typeObject = type
        interactor.presenter = presenter
        router.viewController = view
        
        return view
        
    }
    
}
