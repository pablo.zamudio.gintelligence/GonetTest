//
//  DetailProtocols.swift
//  GonetTest
//
//  Created by kevin Velazquez Zamudio on 19/12/21.
//

import Foundation

// MARK: Wireframe -
protocol DetailRouterProtocol: class {
    
}

// MARK: Presenter -
protocol DetailPresenterProtocol: class {
    func requestDetail(moviesID: Int, type: String)
    func onSuccessGetDetail(data: DetailResponse, response: HTTPURLResponse, type: String) 
    func onFailGetDetail(error: Error)
        
}

// MARK: Interactor -
protocol DetailInteractorProtocol: class {
    var presenter: DetailPresenterProtocol? { get set }
    func getDetail(movieID: Int, type: String)

}

// MARK:  VIEW -
protocol DetailViewProtocol: class {
    var presenter: DetailPresenterProtocol? { get set }
    func successGetDetail(data: DetailResponse, type: String) 
    func failGetDetail(message: String)
}
