//
//  DetailPresenter.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import Foundation

class DetailPresenter: DetailPresenterProtocol {
    
    weak private var view: DetailViewProtocol?
    var interactor: DetailInteractorProtocol?
    private let router: DetailRouterProtocol?
    
    init(interface: DetailViewProtocol, interactor: DetailInteractorProtocol?, router: DetailRouterProtocol ) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
    func requestDetail(moviesID: Int, type: String) {
        interactor?.getDetail(movieID: moviesID, type: type)
    }
    
    func onSuccessGetDetail(data: DetailResponse, response: HTTPURLResponse, type: String) {
        DispatchQueue.main.async {
            if response.statusCode == 200 {
                self.view?.successGetDetail(data: data, type: type)
            }else{
                self.view?.failGetDetail(message: "Error unknown")
            }
        }
    }
    
    func onFailGetDetail(error: Error) {
        DispatchQueue.main.async {
            self.view?.failGetDetail(message: error.localizedDescription)
        }
    }
    
}
