//
//  DetailEntity.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import Foundation

struct DetailResponse: Codable {
    var adult: Bool?
    var backdrop_path: String?
    var budget: Int?
    var genres: [Genres]?
    var homepage: String?
    var id: Int?
    var imdb_id: String?
    var original_lenguaje: String?
    var original_title: String?
    var overview: String?
    var popularity: Double?
    var poster_path: String?
    var release_date: String?
    var revenue: Int?
    var runtime: Int?
    var status: String?
    var title: String?
    var video: Bool?
    var vote_average: Double?
    var vote_count: Int?
    var original_name: String?
    var name: String?
    var first_air_date: String?
}

struct Genres: Codable {
    var id: Int?
    var name: String?
}


