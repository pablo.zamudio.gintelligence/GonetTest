//
//  ListEntity.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

struct MoviesList: Codable {
    var page: Int?
    var results: [Movies]?
}

struct Movies: Codable {
    var adult: Bool?
    var backdrop_path: String?
    var id: Int?
    var original_lenguaje: String?
    var original_title: String?
    var poster_path: String?
    var vote_count: Int?
    var video: Bool?
    var overview: String?
    var release_date: String?
    var vote_average: Double?
    var title: String?
    var popularity: Double?
}

struct TVList: Codable {
    var page: Int?
    var results: [TVShows]?
}

struct TVShows: Codable {
    var backdrop_path: String?
    var id: Int?
    var original_lenguaje: String?
    var original_name: String?
    var poster_path: String?
    var vote_count: Int?
    var video: Bool?
    var overview: String?
    var first_air_date: String?
    var vote_average: Double?
    var name: String?
    var popularity: Double?
}

