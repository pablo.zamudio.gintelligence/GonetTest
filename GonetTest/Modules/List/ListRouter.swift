//
//  ListRouter.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation
import UIKit

open class ListRouter: ListRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static public func getController(sessionID: String) -> UIViewController {
        
       // Generating module components
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: InitViewController.self))
        let view: ListViewController = storyboard.instantiateViewController(identifier: "ListView") as! ListViewController
        let interactor = ListInteractor()
        let router = ListRouter()
        let presenter = ListPresenter(interface: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        view.sessionID = sessionID
        interactor.presenter = presenter
        router.viewController = view
        
        return view
        
    }
    
}
