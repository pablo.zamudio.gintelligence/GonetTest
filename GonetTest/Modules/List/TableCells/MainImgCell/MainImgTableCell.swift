//
//  MainImgTableCell.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import UIKit

class MainImgTableCell: UITableViewCell {
    
// MARK: @IBOUTLETS -
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var blurView: UIView!
    
// MARK: LIFE CYCLE CELL FUNCTIONS -
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
