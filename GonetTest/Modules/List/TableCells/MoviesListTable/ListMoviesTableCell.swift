//
//  ListMoviesTableCell.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import UIKit

protocol CollectionCellDelegate: AnyObject {
    func selectedItem(movieID: Int, type: String)
}

class ListMoviesTableCell: UITableViewCell {
    
// MARK: @IBOUTLETS -
    @IBOutlet weak var lblSection: UILabel!
    @IBOutlet weak var movieCollection: UICollectionView!
    
    var movies: [Movies] = []
    var shows: [TVShows] = []
    var globalType = "Unspecified"
    
    weak var delegate: CollectionCellDelegate?

// MARK: LIFE CYCLE CELL FUNCTIONS -
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
// MARK: SETUP FUNCTIONS -
    func setupCollection(list: [Movies], type: String) {
        movies = list
        globalType = type
        print("$$$", list)
        movieCollection.register(UINib(nibName: "MoviesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "MOVIECELL")
        movieCollection.delegate = self
        movieCollection.dataSource = self
        movieCollection.reloadData()
    }
    
    func setupCollectionTV(list: [TVShows], type: String) {
        shows = list
        globalType = type
        print("$$$", list)
        movieCollection.register(UINib(nibName: "MoviesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "MOVIECELL")
        movieCollection.delegate = self
        movieCollection.dataSource = self
        movieCollection.reloadData()
    }
    
// MARK: @OBJ FUNC -
    @objc func didPressMovie(sender: UIButton) {
        print(sender.tag)
        switch globalType {
        case "MOVIE":
            self.delegate?.selectedItem(movieID: movies[sender.tag].id ?? 0, type: "movie")
            
        case "TV":
            self.delegate?.selectedItem(movieID: shows[sender.tag].id ?? 0, type: "tv")
            
        default:
            break
        }
        
    }

}

extension ListMoviesTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        switch globalType {
        case "MOVIE":
            count = movies.count
            
        case "TV":
            count = shows.count
            
        default:
            break
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MOVIECELL", for: indexPath) as! MoviesCollectionCell
        cell.movieImg.layer.cornerRadius = 7
        cell.btnMovie.tag = indexPath.item
        cell.btnMovie.addTarget(self, action: #selector(didPressMovie), for: .touchUpInside)
        
        switch globalType {
        case "MOVIE":
            cell.movieImg.DownloadImgFromURL(uri: "https://image.tmdb.org/t/p/w500\(movies[indexPath.item].poster_path ?? "")")
            cell.lblNameMovie.text = movies[indexPath.item].title
            cell.lblNameMovie.adjustsFontSizeToFitWidth = true
            let dateMovie = movies[indexPath.item].release_date?.getDate(originalFormat: "yyyy-MM-dd")
            let strDate = dateMovie?.getDateStr(newFormatt: "MMM d, yyyy")
            cell.lblDateMovie.text = strDate?.uppercased()
            
        case "TV":
            if indexPath.item < shows.count {
            cell.movieImg.DownloadImgFromURL(uri: "https://image.tmdb.org/t/p/w500\(shows[indexPath.item].poster_path ?? "")")
            cell.lblNameMovie.text = shows[indexPath.item].name
            cell.lblNameMovie.adjustsFontSizeToFitWidth = true
            let dateMovie = shows[indexPath.item].first_air_date?.getDate(originalFormat: "yyyy-MM-dd")
            let strDate = dateMovie?.getDateStr(newFormatt: "MMM d, yyyy")
            cell.lblDateMovie.text = strDate?.uppercased()
            }
            
        default:
            break
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 190.0, height: 279.0)
    }
    
    
}
