//
//  ListProtocols.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

// MARK: Wireframe -
protocol ListRouterProtocol: class {
    
}

// MARK: Presenter -
protocol ListPresenterProtocol: class {
    func requestFavoriteMovies(sessionID: String, index: Int, type: String)
    func onSuccessMovies(data: MoviesList, response: HTTPURLResponse, index: Int)
    func onFailMovie(error: Error)

    func requestTvShows(sessionID: String, index: Int, type: String) 
    func onSuccessTVShows(data: TVList, response: HTTPURLResponse, index: Int)
    func onFailTvShows(error: Error)
    
}

// MARK: Interactor -
protocol ListInteractorProtocol: class {
    var presenter: ListPresenterProtocol? { get set }
    func getMovies(sessionID: String, index: Int, type: String)
    func getTVShows(sessionID: String, index: Int, type: String)
}

// MARK:  VIEW -
protocol ListViewProtocol: class {
    var presenter: ListPresenterProtocol? { get set }
    func successGetMovies(data: MoviesList, index: Int)
    func failGetMovies(message: String)
    
    func successGetTV(data: TVList, index: Int) 
    func failGetTV(message: String)
}
