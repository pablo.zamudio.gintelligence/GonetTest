//
//  MoviesCollectionCell.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import UIKit

class MoviesCollectionCell: UICollectionViewCell {

// MARK: @IBOUTLETS -
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var lblNameMovie: UILabel!
    @IBOutlet weak var lblDateMovie: UILabel!
    @IBOutlet weak var btnMovie: UIButton!
    
// MARK: LIFE CYCLE CELL FUNCTIONS -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
