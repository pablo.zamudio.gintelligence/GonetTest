//
//  ListPresenter.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

class ListPresenter: ListPresenterProtocol {
    
    weak private var view: ListViewProtocol?
    var interactor: ListInteractorProtocol?
    private let router: ListRouterProtocol?
    
    init(interface: ListViewProtocol, interactor: ListInteractorProtocol?, router: ListRouterProtocol ) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
// MARK: FAVORITE MOVIES -
    func requestFavoriteMovies(sessionID: String, index: Int, type: String) {
        interactor?.getMovies(sessionID: sessionID, index: index, type: type)
    }
    
    func onSuccessMovies(data: MoviesList, response: HTTPURLResponse, index: Int) {
        DispatchQueue.main.async {
            if response.statusCode == 200 {
                self.view?.successGetMovies(data: data, index: index)
            }else{
                self.view?.failGetMovies(message: "Error unknown")
            }
        }
    }
    
    func onFailMovie(error: Error) {
        DispatchQueue.main.async {
            self.view?.failGetMovies(message: error.localizedDescription)
        }
    }
    
// MARK: TV SHOWS -
    func requestTvShows(sessionID: String, index: Int, type: String) {
        interactor?.getTVShows(sessionID: sessionID, index: index, type: type)
    }
    
    func onSuccessTVShows(data: TVList, response: HTTPURLResponse, index: Int) {
        DispatchQueue.main.async {
            if response.statusCode == 200 {
                self.view?.successGetTV(data: data, index: index)
            }else{
                self.view?.failGetTV(message: "Error unknown")
            }
        }
    }
    
    func onFailTvShows(error: Error) {
        DispatchQueue.main.async {
            self.view?.failGetTV(message: error.localizedDescription)
        }
    }
    
}
