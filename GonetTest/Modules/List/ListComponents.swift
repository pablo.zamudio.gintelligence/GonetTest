//
//  ListComponents.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import Foundation
import UIKit

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfSections.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MAINIMGCELL", for: indexPath) as! MainImgTableCell
            return cell
        default:
            let movieCell = tableView.dequeueReusableCell(withIdentifier: "MOVIESLISTCELL", for: indexPath) as! ListMoviesTableCell
            movieCell.delegate = self
            switch typeList[indexPath.row - 1] {
            case "MOVIE":
                movieCell.setupCollection(list: arrayMovies[indexPath.row - 1], type: typeList[indexPath.row - 1])
                movieCell.lblSection.text = arrayOfSections[indexPath.row - 1]
                
            case "TV":
                print(arrayTV.count, indexPath.row - 1)
                movieCell.setupCollectionTV(list: arrayTV[indexPath.row - 1], type: typeList[indexPath.row - 1])
                movieCell.lblSection.text = arrayOfSections[indexPath.row - 1]
                
            default:
                break
            }
           
            
            return movieCell
        }
    }
    
    // PROTOCOLOS PARA QUE LOS COLLECTION VIEW NO SE MUEVAN VARIOS AL MISMO TIEMPO

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        xOffsets[indexPath] = (cell as? ListMoviesTableCell)?.movieCollection.contentOffset.x
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as? ListMoviesTableCell)?.movieCollection.contentOffset.x = xOffsets[indexPath] ?? 0
    }
    
}
