//
//  ListInteractor.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import Foundation

class ListInteractor: ListInteractorProtocol {
    weak var presenter: ListPresenterProtocol?
    
    func getMovies(sessionID: String, index: Int, type: String) {
        guard let apiURL = URL(string: "https://api.themoviedb.org/3/account/0/\(type)/movies?api_key=641f683f7f3e37d02c0392e05520931d&session_id=\(sessionID)&language=en-US&sort_by=created_at.asc&page=1") else { return }
        var request = URLRequest(url: apiURL)
        
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            print(response)
            do {
                if data != nil {
                    let contetResponse = try JSONDecoder().decode(MoviesList.self, from: data!)
                    self.presenter?.onSuccessMovies(data: contetResponse, response: (response as! HTTPURLResponse), index: index)
                }
                
            }catch{
                print("Error get token", error, error.localizedDescription)
                self.presenter?.onFailMovie(error: error)
            }
        }.resume()
    }
    
    func getTVShows(sessionID: String, index: Int, type: String) {
        guard let apiURL = URL(string: "https://api.themoviedb.org/3/account/0/\(type)/tv?api_key=641f683f7f3e37d02c0392e05520931d&language=en-US&session_id=\(sessionID)&sort_by=created_at.asc&page=1") else { return }
        
        var request = URLRequest(url: apiURL)
        
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            print(response)
            do {
                if data != nil {
                    let contetResponse = try JSONDecoder().decode(TVList.self, from: data!)
                    self.presenter?.onSuccessTVShows(data: contetResponse, response: (response as! HTTPURLResponse), index: index)
                    
                }
                
            }catch{
                print("Error get token", error, error.localizedDescription)
                self.presenter?.onFailTvShows(error: error)
            }
        }.resume()
    }
    
}
