//
//  ListViewController.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 18/12/21.
//

import UIKit
import CoreData
import SystemConfiguration

class ListViewController: UIViewController, ListViewProtocol, CollectionCellDelegate {

// MARK: PROTOCOL VAR -
    var presenter: ListPresenterProtocol?
    
// MARK: @IBOUTLETS -
    @IBOutlet weak var mainTable: UITableView!
    
// MARK: LOCAL VAR -
    var sessionID = "Usnpecified"
    var arrayOfSections: [String] = []
    var arrayMovies: [[Movies]] = []
    var emptyMovies: [Movies] = []
    var arrayTV: [[TVShows]] = []
    var emptyTV: [TVShows] = []
    var typeList: [String] = []
    var xOffsets: [IndexPath: CGFloat] = [:]
    
// MARK: - FUNCION CORE DATA
    func conexion()-> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }
    
// MARK: LIFE CYCLE VIEW FUNCTIONS -
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegates()
        validateState(state: sessionID)
    }
    
// MARK: GENERAL FUNCTIONS -
    private func iterateMovieArray(fromArray: [MoviesDB], sectionName: String) {
        if fromArray.count == 0 {
            print("DEBUG: ARRAY IS EMPTY")
        }else{
            arrayOfSections.append(sectionName)
            var newListMovies: [Movies] = []
            fromArray.forEach { (mov) in
                let item = Movies.init(adult: false, backdrop_path: "", id: 0, original_lenguaje: "", original_title: "", poster_path: mov.urlImg, vote_count: 0, video: false, overview: "", release_date: mov.movieDate, vote_average: 0.0, title: mov.movieName, popularity: 0.0)
                newListMovies.append(item)
            }
            arrayMovies.append(newListMovies)
            arrayTV.append(emptyTV)
            typeList.append("MOVIE")
        }
    }
    
    private func iterateTVArray(fromArray: [MoviesDB], sectionName: String) {
        if fromArray.count == 0 {
            print("DEBUG: ARRAY IS EMPTY")
        }else{
            arrayOfSections.append(sectionName)
            var newListTV: [TVShows] = []
            fromArray.forEach { (tv) in
                let item = TVShows.init(backdrop_path: "", id: 0, original_lenguaje: "", original_name: "", poster_path: tv.urlImg, vote_count: 0, video: false, overview: "", first_air_date: tv.movieDate, vote_average: 0.0, name: tv.movieName, popularity: 0.0)
                newListTV.append(item)
            }
            arrayTV.append(newListTV)
            typeList.append("TV")
        }
    }
    
    func showAlert(title: String, text: String, btnTitle: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let btn = UIAlertAction(title: btnTitle, style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(btn)
        self.present(alert, animated: true, completion: nil)
    }
    
// MARK: VALIDATE FUNCTIONS -
    private func validateState(state: String) {
        switch state {
        case "MovieDB":
            print("Form coreData")
            useDataSave()
        default:
            presenter?.requestFavoriteMovies(sessionID: sessionID, index: 0, type: "favorite")
        }
    }
    
// MARK: SETUP FUNCTIONS -
    private func setupDelegates() {
        mainTable.delegate = self
        mainTable.dataSource = self
    }
    
    func selectedItem(movieID: Int, type: String) {
        if CheckInternet.Connection() {
            let module = DetailRouter.getController(movieID: movieID, type: type)
            self.present(module, animated: true, completion: nil)
        }else{
            showAlert(title: "Aviso", text: "no cuentas con conexión a internet", btnTitle: "Entendido")
        }
        
    }
    
// MARK: COREDATA FUNCTIONS -
    func deleteDB() {
        let contexto = conexion()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MoviesDB")
        let borrar = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try contexto.execute(borrar)
            print("Exito al borrar DB")
        } catch let error as NSError{
            print("Error al eliminar DB", error.localizedDescription)
        }
    }
    
    func saveMovie(url: String, name: String, date: String, type: String) {
        let contexto = self.conexion()
        let entidadMovies = NSEntityDescription.insertNewObject(forEntityName: "MoviesDB", into: contexto) as! MoviesDB
        entidadMovies.urlImg = url
        entidadMovies.movieName = name
        entidadMovies.movieDate = date
        entidadMovies.typeObject = type
        
        do {
            try contexto.save()
            print("Exito al guardar token")
        }catch let error as NSError{
            print("Error al guardar token", error.localizedDescription)
        }
    }
    
    func useDataSave() {
        let contexto = self.conexion()
        let fetchRequest : NSFetchRequest<MoviesDB> = MoviesDB.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            
            let selectedItem : Set<String> = ["MOVIE"]
            let selectedMovies = resultados.filter({selectedItem.contains($0.typeObject ?? "")})
            iterateMovieArray(fromArray: selectedMovies, sectionName: "Favorite Movies")
            
            let selectedRated: Set<String> = ["MOVIERA"]
            let moviesRated = resultados.filter({selectedRated.contains($0.typeObject ?? "")})
            iterateMovieArray(fromArray: moviesRated, sectionName: "Rated Movies")
            
            let selectedRecommended: Set<String> = ["MOVIERE"]
            let moviesRecommended = resultados.filter({selectedRecommended.contains($0.typeObject ?? "")})
            iterateMovieArray(fromArray: moviesRecommended, sectionName: "Recommended Movies")
            
            let selectedTV: Set<String> = ["TVSHOWF"]
            let tv = resultados.filter({selectedTV.contains($0.typeObject ?? "")})
            iterateTVArray(fromArray: tv, sectionName: "Favorite TV Shows")
            
            let selectedTVRated: Set<String> = ["TVSHOWRA"]
            let tvRated = resultados.filter({selectedTVRated.contains($0.typeObject ?? "")})
            iterateTVArray(fromArray: tvRated, sectionName: "Rated TV Shows")
            
            let selectedTVRecommended: Set<String> = ["TVSHOWRE"]
            let tvRecommended = resultados.filter({selectedTVRecommended.contains($0.typeObject ?? "")})
            iterateTVArray(fromArray: tvRecommended, sectionName: "Recommended TV Shows")
            
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
        }
    }

}

// MARK: SERVICES RESPONSE -
extension ListViewController {
    //MOVIES
    func successGetMovies(data: MoviesList, index: Int) {
        print(data)
        
        switch index {
        case 0:
            deleteDB()
            arrayOfSections.append("Favorite Movies")
            arrayMovies.append(data.results ?? emptyMovies)
            arrayTV.append(emptyTV)
            typeList.append("MOVIE")
            
            data.results?.forEach({ (movie) in
                saveMovie(url: "https://image.tmdb.org/t/p/w500\(movie.poster_path ?? "")", name: movie.title ?? "", date: movie.release_date ?? "", type: "MOVIE")
            })
            
            presenter?.requestFavoriteMovies(sessionID: sessionID, index: 1, type: "rated")
            
        case 1:
            arrayOfSections.append("Rated Movies")
            arrayMovies.append(data.results ?? emptyMovies)
            typeList.append("MOVIE")
            arrayTV.append(emptyTV)
            data.results?.forEach({ (movie) in
                saveMovie(url: "https://image.tmdb.org/t/p/w500\(movie.poster_path ?? "")", name: movie.title ?? "", date: movie.release_date ?? "", type: "MOVIERA")
            })
            presenter?.requestFavoriteMovies(sessionID: sessionID, index: 2, type: "watchlist")
            
        case 2:
            arrayOfSections.append("Recommended Movies")
            arrayMovies.append(data.results ?? emptyMovies)
            typeList.append("MOVIE")
            arrayTV.append(emptyTV)
            data.results?.forEach({ (movie) in
                saveMovie(url: "https://image.tmdb.org/t/p/w500\(movie.poster_path ?? "")", name: movie.title ?? "", date: movie.release_date ?? "", type: "MOVIERE")
            })
            presenter?.requestTvShows(sessionID: sessionID, index: 0, type: "favorite")
            
        default:
            break
        }
        
        mainTable.reloadData()
    }
    
    func failGetMovies(message: String) {
        print(message)
    }
    
    // TV Shows
    func successGetTV(data: TVList, index: Int) {
        switch index {
        case 0:
            arrayOfSections.append("Favorite TV Shows")
            arrayTV.append(data.results ?? emptyTV)
            typeList.append("TV")
            data.results?.forEach({ (tv) in
                saveMovie(url: "https://image.tmdb.org/t/p/w500\(tv.poster_path ?? "")", name: tv.name ?? "", date: tv.first_air_date ?? "", type: "TVSHOWF")
            })
            presenter?.requestTvShows(sessionID: sessionID, index: 1, type: "rated")
            
        case 1:
            arrayOfSections.append("Rated TV Shows")
            arrayTV.append(data.results ?? emptyTV)
            typeList.append("TV")
            data.results?.forEach({ (tv) in
                saveMovie(url: "https://image.tmdb.org/t/p/w500\(tv.poster_path ?? "")", name: tv.name ?? "", date: tv.first_air_date ?? "", type: "TVSHOWRA")
            })
            presenter?.requestTvShows(sessionID: sessionID, index: 2, type: "watchlist")
            
        case 2:
            arrayOfSections.append("Recommended TV Shows")
            arrayTV.append(data.results ?? emptyTV)
            typeList.append("TV")
            data.results?.forEach({ (tv) in
                saveMovie(url: "https://image.tmdb.org/t/p/w500\(tv.poster_path ?? "")", name: tv.name ?? "", date: tv.first_air_date ?? "", type: "TVSHOWRE")
            })
            
        default:
            break
        }
        
        mainTable.reloadData()
    }
    
    func failGetTV(message: String) {
        
    }
    
}

