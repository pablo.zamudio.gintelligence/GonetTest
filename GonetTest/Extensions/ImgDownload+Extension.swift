//
//  ImgDownload+Extension.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import Foundation
import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()
var gradientLayer : CAGradientLayer!

extension UIImageView {
    
    func DownloadImgFromURL(uri : String) {
        guard let url = URL(string: uri) else { return }
        
        image = nil
        
        if let imageFormCache = imageCache.object(forKey: uri as AnyObject) as? UIImage {
            self.image = imageFormCache
            
            return
        }
        
            let task = URLSession.shared.dataTask(with: url) {responseData,response,error in
                if error == nil {
                    if let data = responseData {
                        
                        DispatchQueue.main.async {
                            
                            let imageToCache = UIImage(data: data)
                            if imageToCache != nil {
                                imageCache.setObject(imageToCache!, forKey: uri as AnyObject)
                                self.image = imageToCache
                                
                            }
                            
                        }
                        
                    }else {
                        print("no data")
                    }
                }else{
                    print("error")
                }
            }
            task.resume()
        
    }
    
    func DownloadStaticImage(_ uri : String) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        self.image = UIImage(data: data)
                        
                        print("Fin del hilo imagen muestra")
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
    
}
