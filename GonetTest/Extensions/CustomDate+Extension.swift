//
//  CustomDate+Extension.swift
//  GonetTest
//
//  Created by Kevin Velazquez Zamudio on 19/12/21.
//

import Foundation
import UIKit

extension String {
    func getDate(originalFormat: String) -> Date? {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = originalFormat
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = Locale.current
            return dateFormatter.date(from: self) // replace Date String
        }
}

extension Date {
    func getDateStr(newFormatt: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "Es_MX")
        dateFormatter.dateFormat = newFormatt //EEEE, MMM d, yyyy
        return dateFormatter.string(from: self)
    }
}
